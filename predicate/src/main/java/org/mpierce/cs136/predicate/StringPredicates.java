package org.mpierce.cs136.predicate;

import java.util.ArrayList;
import java.util.List;

public class StringPredicates {

    static List<String> filter(List<String> strings, StringPredicate predicate) {
        List<String> result = new ArrayList<>();

        for (String s : strings) {
            if (predicate.evaluate(s)) {
                result.add(s);
            }
        }

        return result;
    }
}
