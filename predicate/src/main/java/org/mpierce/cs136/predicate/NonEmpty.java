package org.mpierce.cs136.predicate;

public class NonEmpty implements StringPredicate {
    @Override
    public boolean evaluate(String s) {
        return !s.isEmpty();
    }
}
