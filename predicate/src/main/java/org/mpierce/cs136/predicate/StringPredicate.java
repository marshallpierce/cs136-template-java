package org.mpierce.cs136.predicate;

import java.util.List;

/**
 * For use with {@link StringPredicates#filter(List, StringPredicate)}.
 */
public interface StringPredicate {

    /**
     * @return true if the string should be considered to be selected by the predicate
     */
    boolean evaluate(String s);
}
