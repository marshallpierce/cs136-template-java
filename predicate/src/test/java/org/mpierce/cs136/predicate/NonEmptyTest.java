package org.mpierce.cs136.predicate;

import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NonEmptyTest {
    @Test
    void filterEmptyListReturnsEmptyList() {
        assertEquals(List.of(), StringPredicates.filter(List.of(), new NonEmpty()));
    }

    @Test
    void filterRemovesEmptyStrings() {
        assertEquals(List.of("foo", " ", "bar"),
                StringPredicates.filter(List.of("foo", "", " ", "bar"), new NonEmpty()));
    }
}
