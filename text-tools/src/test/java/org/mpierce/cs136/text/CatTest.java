package org.mpierce.cs136.text;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CatTest {

    @Test
    void printsAllLines() throws IOException {
        StringWriter writer = new StringWriter();
        feedLinesToHandler(new Cat(writer), List.of("foo", "bar", "", "baz"));

        assertEquals("foo\nbar\n\nbaz\n", writer.toString());
    }

    static void feedLinesToHandler(LineHandler handler, List<String> lines) throws IOException {
        for (String line : lines) {
            handler.onLine(line);
        }

        handler.onEnd();
    }
}
