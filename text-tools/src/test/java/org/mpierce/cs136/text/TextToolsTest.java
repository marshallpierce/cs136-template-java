package org.mpierce.cs136.text;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mpierce.cs136.text.TextTools.handleAllLines;

class TextToolsTest {

    @Test
    void handleAllLinesGetsAllTheLines() throws IOException {
        List<String> lines = List.of("foo", "bar", "", "baz");
        byte[] bytes = String.join("\n", lines)
                .getBytes(StandardCharsets.UTF_8);

        ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
        LineListHandler handler = new LineListHandler();
        handleAllLines(handler, stream);

        assertEquals(lines, handler.lines);
    }

    private static class LineListHandler implements LineHandler {

        private final List<String> lines = new ArrayList<>();

        @Override
        public void onLine(String line) {
            lines.add(line);
        }

        @Override
        public void onEnd() {
            // no op
        }
    }
}
