package org.mpierce.cs136.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class TextTools {
    public static void main(String[] args) throws IOException {
        try (OutputStreamWriter stdoutWriter = new OutputStreamWriter(System.out, StandardCharsets.UTF_8)) {
            LineHandler handler = getLineHandler(args, stdoutWriter);

            if (handler == null) {
                System.err.println("Unknown command: " + args[0]);
                System.exit(1);
            }

            // read stdin lines

            handleAllLines(handler, System.in);
        }
    }

    static void handleAllLines(LineHandler handler, InputStream in) throws IOException {
        try (var reader = new InputStreamReader(in, StandardCharsets.UTF_8)) {
            try (var bufReader = new BufferedReader(reader)) {
                String line = bufReader.readLine();

                while (line != null) {
                    handler.onLine(line);
                    line = bufReader.readLine();
                }

                handler.onEnd();
            }
        }
    }

    private static LineHandler getLineHandler(String[] args, OutputStreamWriter stdoutWriter) {
        switch (args[0]) {
            case "cat":
                return new Cat(stdoutWriter);
            default:
                return null;
        }
    }
}
