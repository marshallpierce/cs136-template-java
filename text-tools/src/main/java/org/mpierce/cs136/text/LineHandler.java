package org.mpierce.cs136.text;

import java.io.IOException;

public interface LineHandler {
    void onLine(String line) throws IOException;

    void onEnd() throws IOException;
}
