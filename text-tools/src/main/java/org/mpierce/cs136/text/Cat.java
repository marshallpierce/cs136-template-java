package org.mpierce.cs136.text;

import java.io.IOException;
import java.io.Writer;

final class Cat implements LineHandler {

    private final Writer out;

    Cat(Writer out) {
        this.out = out;
    }

    @Override
    public void onLine(String line) throws IOException {
        out.append(line);
        out.append("\n");
    }

    @Override
    public void onEnd() {
        // no op
    }
}
