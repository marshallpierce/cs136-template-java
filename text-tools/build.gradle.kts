plugins {
    application
}

val deps: Map<String, String> by extra

application {
    mainClassName = "org.mpierce.cs136.text.TextTools"
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

// `run` is a kotlin builtin, so we have to use this helper to configure the `run` task
tasks.named<JavaExec>("run") {
    // hook gradle's stdin to the above main class when it runs so that it can be meaningfully run via gradle
    standardInput = System.`in`
}
