package edu.umontana.cs136.homework;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DummyClassTest {
    @Test
    void meaningOfLifeIsCorrect() {
        assertEquals(42, new DummyClass().meaningOfLife());
    }
}
