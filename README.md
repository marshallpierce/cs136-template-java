Demo project for CS 136 students.

- Follow OS setup steps below.
- Clone this repo.
- Run `./gradlew check` in your clone (or, on Windows, use `gradlew.bat` instead of `gradlew`). This will download and run the build tool, compile the source, and run the tests.
- Follow IDE setup steps below.

# Submodules

- `hello-world` - just Hello World. `./gradlew :hello-world:run`
- `predicate` - A demo of using interfaces to filter strings. Try running `NonEmptyTest` from IntelliJ and trace through the logic.
- `text-tools` - Starting point for a Java version of the basic text tools we've already built in Python. Try `jot 10 | ./gradlew :text-tools:run --args='cat'`.
- `homework` - A place to put your homework.

# OS Setup

## Linux and macOS

We'll use [sdkman](https://sdkman.io/) to manage JDK (Java Development Kit) installations.

- [Install sdkman](https://sdkman.io/install)
- Once you have `sdk` available in your shell, list the available java versions: `sdk list java`
- You should see something like this:
    
```
     13.ea.07-open       8.0.202-amzn
     12.ea.31-open       8.0.202.j9-adpt
     11.0.2-sapmchn      8.0.202.hs-adpt
 > * 11.0.2-zulu         8.0.202-zulufx
     11.0.2-open         8.0.201-oracle
     11.0.2-amzn         7.0.181-zulu
     11.0.2.j9-adpt      1.0.0-rc-12-grl
     11.0.2.hs-adpt      1.0.0-rc-11-grl
     11.0.2-zulufx       1.0.0-rc-10-grl
   + 11.0.1-open         1.0.0-rc-9-grl
   * 10.0.2-zulu         1.0.0-rc-8-grl
     10.0.2-open
     9.0.7-zulu
     9.0.4-open
     8.0.202-zulu

================================================================================
+ - local version
* - installed
> - currently in use
================================================================================
```

- Install the latest version of the `zulu` jdk, here 11.0.2: `sdk install java 11.0.2-zulu`
- Once that's done, `java -version` should show the following:

```
openjdk version "11.0.2" 2019-01-15 LTS
OpenJDK Runtime Environment Zulu11.29+3-CA (build 11.0.2+7-LTS)
OpenJDK 64-Bit Server VM Zulu11.29+3-CA (build 11.0.2+7-LTS, mixed mode)
```

## Windows

Download the [Zulu build](https://www.azul.com/downloads/zulu/zulu-windows/) of the latest Java 11 release (currently, 11.0.2). The MSI installer is probably easier to use but you can use the ZIP packaging if you prefer.

Without sdkman, you won't be able to easily manage having multiple versions of Java, but at least you can have one...

# IDE setup

- Install IntelliJ Community via the Jetbrains Toolbox app.
- Run IntelliJ and open this repo's directory.
    - It should auto detect that it's a Gradle project.
    - During this process, since it's your first time using it, IntelliJ will likely complain that it doesn't yet have a JDK (Java Development Kit) set up. You will need to create one by pointing IntelliJ at wherever the Java package you installed above is. 
        - If you used sdkman, the different java versions you have (probably just one at this point) are in `~/.sdkman/candidates/java`. Select the directory containing the version you want (e.g. 11.0.2-zulu) and IntelliJ should figure out that it's a JDK install.
        - If you used Windows, probably somewhere in Program Files? Or, wherever you put the zip contents.
- Once you've clicked through the new project steps, you should be able to open `HelloWorld` in the `hello-world` submodule and run it (right click -> Run).
    - Also try running via the command line: `./gradlew :hello-world:run`
