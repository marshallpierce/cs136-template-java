
subprojects {
    apply<JavaPlugin>()

    repositories {
        jcenter()
    }

    // shared dependency versions
    val deps by extra {
        mapOf(
                "junit" to "5.4.2"
        )
    }

    // config is a little clunky because kotlin dsl still has some quirks when in a subprojects block

    configure<JavaPluginExtension> {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    tasks.named<Test>("test") {
        useJUnitPlatform()
    }
}

