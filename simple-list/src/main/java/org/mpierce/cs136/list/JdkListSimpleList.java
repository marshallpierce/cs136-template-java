package org.mpierce.cs136.list;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * A SimpleList implemented with the JDK's List.
 */
public class JdkListSimpleList<T> implements SimpleList<T> {

    private final List<T> list;

    JdkListSimpleList(List<T> emptyList) {
        this.list = emptyList;
    }

    @Override
    public void addFirst(T element) {
        list.add(0, element);
    }

    @Override
    public void addLast(T element) {
        list.add(element);
    }

    @Override
    public T get(int index) {
        return list.get(index);
    }

    @Override
    public T removeFirst() {
        return list.remove(0);
    }

    @Override
    public T removeLast() {
        return list.remove(list.size() - 1);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public <U> SimpleList<U> map(Function<T, U> function) {
        return new JdkListSimpleList<>(list.stream()
                .map(function)
                .collect(Collectors.toList()));
    }

    @Override
    public SimpleList<T> filter(Predicate<T> pred) {
        return new JdkListSimpleList<>(list.stream()
                .filter(pred)
                .collect(Collectors.toList()));
    }
}
