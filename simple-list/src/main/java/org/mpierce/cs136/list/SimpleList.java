package org.mpierce.cs136.list;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A simplified list interface.
 *
 * @param <T> the type of the elements in the list
 */
public interface SimpleList<T> {

    /**
     * Add at the start of the list (element will be at index 0)
     *
     * @param element the element to add
     */
    void addFirst(T element);

    /**
     * Add at the end of the list (element will be at the last index)
     *
     * @param element the element to add
     */
    void addLast(T element);

    /**
     * @param index position in the list, starting at 0
     * @return the element at the provided index
     * @throws IndexOutOfBoundsException if the index is out of bounds
     */
    T get(int index);

    /**
     * @return the formerly first element
     * @throws IndexOutOfBoundsException if the list is empty
     */
    T removeFirst();

    /**
     * @return the formerly last element
     * @throws IndexOutOfBoundsException if the list is empty
     */
    T removeLast();

    /**
     * @return the number of elements in the list
     */
    int size();

    /**
     * @param function the function to transform individual elements
     * @param <U>      the new type
     * @return a list containing the result of applying function to each element
     */
    <U> SimpleList<U> map(Function<T, U> function);

    /**
     * @param pred the predicate applied to each element
     * @return a list containing the elements for which pred returned true
     */
    SimpleList<T> filter(Predicate<T> pred);
}
