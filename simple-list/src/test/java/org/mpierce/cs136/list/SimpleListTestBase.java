package org.mpierce.cs136.list;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for any SimpleList
 */
abstract class SimpleListTestBase {
    abstract <T> SimpleList<T> getNewList();

    @Test
    void emptyListHasSize0() {
        assertEquals(0, getNewList().size());
    }

    @Test
    void getOnEmptyListThrows() {
        assertThrows(IndexOutOfBoundsException.class, () -> getNewList().get(0));
    }

    @Test
    void removeFirstOnEmptyListThrows() {
        assertThrows(IndexOutOfBoundsException.class, () -> getNewList().removeFirst());
    }

    @Test
    void removeLastOnEmptyListThrows() {
        assertThrows(IndexOutOfBoundsException.class, () -> getNewList().removeLast());
    }

    @Test
    void listWithOneElementHasSize1() {
        SimpleList<Integer> sl = getNewList();
        sl.addFirst(1);

        assertEquals(1, sl.size());
    }

    @Test
    void listWithOneElementCanGetElement() {
        SimpleList<Integer> sl = getNewList();
        sl.addFirst(1);

        assertEquals(1, sl.get(0));
    }

    @Test
    void addFirstAddsAtBeginning() {
        SimpleList<Integer> sl = getNewList();

        sl.addFirst(1);
        sl.addFirst(2);
        sl.addFirst(3);

        assertEquals(List.of(3, 2, 1), toList(sl));
    }

    @Test
    void addLastAddsAtEnd() {
        SimpleList<Integer> sl = getNewList();

        sl.addLast(1);
        sl.addLast(2);
        sl.addLast(3);

        assertEquals(List.of(1, 2, 3), toList(sl));
    }

    @Test
    void getInBounds() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertEquals(1, sl.get(0));
        assertEquals(2, sl.get(1));
        assertEquals(3, sl.get(2));
    }

    @Test
    void getOutOfBounds() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertThrows(IndexOutOfBoundsException.class, () -> sl.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> sl.get(sl.size()));
        assertThrows(IndexOutOfBoundsException.class, () -> sl.get(10000));
    }

    @Test
    void removeFirst() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertEquals(List.of(1, 2, 3), toList(sl));
        assertEquals(3, sl.size());

        assertEquals(1, sl.removeFirst());
        assertEquals(2, sl.size());
        assertEquals(List.of(2, 3), toList(sl));

        assertEquals(2, sl.removeFirst());
        assertEquals(1, sl.size());
        assertEquals(List.of(3), toList(sl));

        assertEquals(3, sl.removeFirst());
        assertEquals(0, sl.size());
        assertEquals(List.of(), toList(sl));
    }

    @Test
    void removeLast() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertEquals(List.of(1, 2, 3), toList(sl));
        assertEquals(3, sl.size());

        assertEquals(3, sl.removeLast());
        assertEquals(2, sl.size());
        assertEquals(List.of(1, 2), toList(sl));

        assertEquals(2, sl.removeLast());
        assertEquals(1, sl.size());
        assertEquals(List.of(1), toList(sl));

        assertEquals(1, sl.removeLast());
        assertEquals(0, sl.size());
        assertEquals(List.of(), toList(sl));
    }

    @Test
    void mapMaps() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertEquals(List.of(1, 4, 9), toList(sl.map(x -> x * x)));
    }

    @Test
    void filterFilters() {
        SimpleList<Integer> sl = toSimpleList(List.of(1, 2, 3));

        assertEquals(List.of(1, 3), toList(sl.filter(x -> x % 2 != 0)));
    }

    private <T> List<T> toList(SimpleList<T> sl) {
        List<T> list = new ArrayList<>();

        for (int i = 0; i < sl.size(); i++) {
            list.add(sl.get(i));
        }

        return list;
    }

    private <T> SimpleList<T> toSimpleList(List<T> list) {
        SimpleList<T> sl = getNewList();

        for (T t : list) {
            sl.addLast(t);
        }

        return sl;
    }
}
