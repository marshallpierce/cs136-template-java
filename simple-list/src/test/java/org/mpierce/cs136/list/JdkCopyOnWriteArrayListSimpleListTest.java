package org.mpierce.cs136.list;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Just to show we can have multiple, here it is also for CopyOnWriteArrayList, yet another List implementation.
 */
class JdkCopyOnWriteArrayListSimpleListTest extends SimpleListTestBase {
    @Override
    <T> SimpleList<T> getNewList() {
        return new JdkListSimpleList<>(new CopyOnWriteArrayList<>());
    }
}
