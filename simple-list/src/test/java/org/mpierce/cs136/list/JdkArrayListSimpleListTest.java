package org.mpierce.cs136.list;

import java.util.ArrayList;

/**
 * Allows running {@link SimpleListTestBase} for JdkListSimpleList with an ArrayList
 */
class JdkArrayListSimpleListTest extends SimpleListTestBase {
    @Override
    <T> SimpleList<T> getNewList() {
        return new JdkListSimpleList<>(new ArrayList<>());
    }
}
