package org.mpierce.cs136.example.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class PlainBoxTest {
    @Test
    void getsTheOriginalObject() {
        PlainBox b = new PlainBox("foo");
        assertEquals("foo", b.getThing());
    }

    @Test
    void getWithoutCast() {
        PlainBox b = new PlainBox("foo");
        Object thing = b.getThing();
        assertEquals("foo", thing);
    }

    @Test
    void getWithCast() {
        PlainBox b = new PlainBox("foo");
        String thing = (String) b.getThing();
        assertEquals("foo", thing);
    }

    @Test
    void getWithWrongCast() {
        PlainBox b = new PlainBox("foo");

        try {
            Integer thing = (Integer) b.getThing();
            fail();
        } catch (ClassCastException e) {
            // expected
        }
    }
}
