package org.mpierce.cs136.example.exceptions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.mpierce.cs136.example.exceptions.ExceptionsMain.loadDataFromDisk;
import static org.mpierce.cs136.example.exceptions.ExceptionsMain.squareRoot;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

class ExceptionsTest {

    @Test
    void checkThrowsExplicit() {
        try {
            squareRoot(-1);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Input must be non negative", e.getMessage());
        }
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    void checkThrowsWithAnonInnerClass() {
        assertThrows(IllegalArgumentException.class, new Executable() {
            @Override
            public void execute() {
                squareRoot(-3);
            }
        });
    }

    @Test
    void checkThrowsWithLambda() {
        assertThrows(IllegalArgumentException.class, () -> squareRoot(-3));
    }

    @Test
    void tryCatch() {
        try {
            loadDataFromDisk();
            fail();
        } catch (IOException e) {
            // expected
        }
    }

    @Test
    void tryCatchSupertype() {
        try {
            loadDataFromDisk();
            fail();
        } catch (Exception e) {
            // expected
        }
    }

    @Test
    void trySeparateCatch() {
        try {
            loadDataFromDisk();
            fail();
        } catch (IOException e) {
            // expected
        } catch (IllegalArgumentException e) {
            fail();
        }
    }

    @Test
    void tryUnionCatch() {
        try {
            loadDataFromDisk();
            fail();
        } catch (IOException | IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    void tryFinallyWithException() {
        try {
            loadDataFromDisk();
            fail();
        } catch (IOException e) {
            System.out.println("Got exception");
        } finally {
            System.out.println("Finally running");
        }
    }

    @Test
    void tryFinallyWithNormalCompletion() {
        try {
            System.out.println("Try block completing normally");
        } catch (IllegalArgumentException e) {
            System.out.println("Got exception");
        } finally {
            System.out.println("Finally running");
        }
    }

    @Test
    void tryFinallyCloseResourceTheHardWay() {
        // pretend we had just gotten a stream from a file on disk or a network connection or something
        InputStream stream = new ByteArrayInputStream(new byte[0]);

        // now, use it without leaking it if there's an error
        try {
            int data = stream.read();
            System.out.println("Got some data: " + data);
        } catch (IOException e) {
            System.out.println("Reading failed: " + e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                System.out.println("Couldn't close the stream:" + e);
            }
        }
    }

    @Test
    void tryWithResources() {
        try (InputStream stream = new ByteArrayInputStream(new byte[0])) {
            int data = stream.read();
            System.out.println("Got some data: " + data);
        } catch (IOException e) {
            System.out.println("Reading failed: " + e);
        }
    }
}
