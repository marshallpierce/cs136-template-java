package org.mpierce.cs136.example.generics;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EqualsHashCodeTest {
    @Test
    void justStringsWorks() {
        Set<String> strings = new HashSet<>();

        strings.add("foo");
        strings.add("foo");
        strings.add("bar");
        strings.add("bar");

        assertEquals(2, strings.size());
    }

    @Test
    void stringHolderNoEqualsDoesntWork() {
        Set<StringHolderNoEqualsHashCode> strings = new HashSet<>();

        strings.add(new StringHolderNoEqualsHashCode("foo"));
        strings.add(new StringHolderNoEqualsHashCode("foo"));
        strings.add(new StringHolderNoEqualsHashCode("bar"));
        strings.add(new StringHolderNoEqualsHashCode("bar"));

        assertEquals(4, strings.size());
    }

    @Test
    void stringHolderWithEqualsDoesWork() {
        Set<StringHolderWithEqualsHashCode> strings = new HashSet<>();

        strings.add(new StringHolderWithEqualsHashCode("foo"));
        strings.add(new StringHolderWithEqualsHashCode("foo"));
        strings.add(new StringHolderWithEqualsHashCode("bar"));
        strings.add(new StringHolderWithEqualsHashCode("bar"));

        assertEquals(2, strings.size());
    }
}
