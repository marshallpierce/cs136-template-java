package org.mpierce.cs136.example.generics;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GenericBoxTest {
    @Test
    void getsTheOriginalObject() {
        GenericBox<String> b = new GenericBox<>("foo");
        assertEquals("foo", b.getThing());
        String s = b.getThing();
        assertEquals("foo", s);
    }

    @Test
    void rawTypeWarning() {
        // Using a generic type without a type parameter: a "raw" type
        // The compiler warns about this, and the IDE will also display a warning
        GenericBox b = new GenericBox("foo");
        // still has the right object inside it...
        assertEquals("foo", b.getThing());
        // ... but the compiler doesn't know it's a String, so we have to cast
        String thing = (String) b.getThing();
        assertEquals("foo", thing);
    }
}
