package org.mpierce.cs136.example.lambdas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LambdaTest {
    @Test
    void useTopLevelFunctionImplementation() {
        Function<String, String> truncator = new TopLevelSkipFirstChar();

        assertEquals("oo", truncator.apply("foo"));
    }

    @Test
    void useStaticInnerClassImpl() {
        Function<String, String> truncator = new StaticSkipFirstChar();

        assertEquals("oo", truncator.apply("foo"));
    }

    @SuppressWarnings({"Convert2Diamond", "Convert2Lambda"})
    @Test
    void useAnonInnerClass() {
        Function<String, String> truncator = new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.substring(1);
            }
        };

        assertEquals("oo", truncator.apply("foo"));
    }

    @Test
    void useLambda() {
        Function<String, String> truncator = s -> s.substring(1);

        assertEquals("oo", truncator.apply("foo"));
    }

    @Test
    void mapWithTopLevelClass() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> mapped = orig.stream()
                .map(new TopLevelSkipFirstChar())
                .collect(Collectors.toList());

        assertEquals(List.of("oo", "ar", "az"), mapped);
    }

    @Test
    void mapWithStaticClass() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> mapped = orig.stream()
                .map(new StaticSkipFirstChar())
                .collect(Collectors.toList());

        assertEquals(List.of("oo", "ar", "az"), mapped);
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    void mapWithAnonInnerClass() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> mapped = orig.stream()
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String s) {
                        return s.substring(1);
                    }
                })
                .collect(Collectors.toList());

        assertEquals(List.of("oo", "ar", "az"), mapped);
    }

    @Test
    void mapWithLambda() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> mapped = orig.stream()
                .map(s -> s.substring(1))
                .collect(Collectors.toList());

        assertEquals(List.of("oo", "ar", "az"), mapped);
    }

    @SuppressWarnings({"Convert2Lambda", "Convert2Diamond"})
    @Test
    void filterWithAnonInnerClass() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> filtered = orig.stream()
                .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) {
                        return s.contains("a");
                    }
                })
                .collect(Collectors.toList());

        assertEquals(List.of("bar", "baz"), filtered);
    }

    @Test
    void filterWithLambda() {
        List<String> orig = List.of("foo", "bar", "baz");
        List<String> filtered = orig.stream()
                .filter(s -> s.contains("a"))
                .collect(Collectors.toList());

        assertEquals(List.of("bar", "baz"), filtered);
    }

    @SuppressWarnings({"Convert2Lambda", "Convert2Diamond"})
    @Test
    void listToMapWithAnonInnerClass() {
        Map<String, String> map = toMapSimple(List.of("foo", "bar", "baz"), new Function<String, String>() {
            @Override
            public String apply(String s) {
                return s.substring(1);
            }
        });

        assertEquals(Map.of("foo", "oo", "bar", "ar", "baz", "az"), map);
    }

    @Test
    void listToMapWithLambda() {
        Map<String, String> map = toMapFancy(List.of("foo", "bar", "baz"), s -> s.substring(1));

        assertEquals(Map.of("foo", "oo", "bar", "ar", "baz", "az"), map);
    }

    @Test
    void namedClassThatCapturesState() {
        assertEquals(5, addSomething(2, new AdderWithField(3)));
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    void anonInnerClassThatAdds() {
        assertEquals(5, addSomething(2, new Adder() {
            @Override
            public int addTo(int input) {
                return input + 3;
            }
        }));
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    void anonInnerClassThatCapturesStateAndAdds() {
        int num = 17;
        assertEquals(19, addSomething(2, new Adder() {
            @Override
            public int addTo(int input) {
                return num + input;
            }
        }));
    }

    @Test
    void lambdaThatCapturesAndAdds() {
        int num = 17;
        assertEquals(19, addSomething(2, input -> num + input));
    }

    @SuppressWarnings("SameParameterValue")
    static int addSomething(int input, Adder adder) {
        return adder.addTo(input);
    }

    interface Adder {
        int addTo(int input);
    }

    static class AdderWithField implements Adder {

        private int num;

        AdderWithField(int num) {
            this.num = num;
        }

        @Override
        public int addTo(int input) {
            return input + num;
        }
    }

    static class StaticSkipFirstChar implements Function<String, String> {
        @Override
        public String apply(String s) {
            return s.substring(1);
        }
    }

    static <K, V> Map<K, V> toMapSimple(List<K> list, Function<K, V> valueCalculator) {
        Map<K, V> map = new HashMap<>();
        for (K key : list) {
            map.put(key, valueCalculator.apply(key));
        }

        return map;
    }

    static <K, V> Map<K, V> toMapFancy(List<K> list, Function<K, V> valueCalculator) {
        return list.stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        valueCalculator
                ));
    }
}
