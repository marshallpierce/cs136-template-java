package org.mpierce.cs136.example.lambdas;

import java.util.function.Function;

class TopLevelSkipFirstChar implements Function<String, String> {
    @Override
    public String apply(String s) {
        return s.substring(1);
    }

}
