package org.mpierce.cs136.example.lambdas;

import org.junit.jupiter.api.Test;

class AnotherLambdaTest {

    @Test
    void normalClass() {
        // using an implementation that's a normal class
        DoIt d = new Nike();

        d.doIt();
    }

    @SuppressWarnings("Convert2Lambda")
    @Test
    void anonInnerClass() {
        DoIt d = new DoIt() {
            @Override
            public String doIt() {
                System.out.println("I'm an AIC");
                return "aic return value";
            }
        };

        d.doIt();
    }

    @Test
    void lambda() {
        DoIt d = () -> {
            System.out.println("I'm a lambda");
            System.out.println("I'm still a lambda");

            return "lambda return value";
        };

        d.doIt();
    }

    interface DoIt {
        @SuppressWarnings("UnusedReturnValue")
        String doIt();
    }

    static class Nike implements DoIt {
        @Override
        public String doIt() {
            System.out.println("just do it");
            return "nike";
        }
    }
}
