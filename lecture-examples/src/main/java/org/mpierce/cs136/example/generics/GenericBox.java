package org.mpierce.cs136.example.generics;

public class GenericBox<T> {
    private final T thing;

    public GenericBox(T thing) {
        this.thing = thing;
    }

    T getThing() {
        return this.thing;
    }
}
