package org.mpierce.cs136.example.generics;

class StringHolderNoEqualsHashCode {

    private final String s;

    StringHolderNoEqualsHashCode(String s) {
        this.s = s;
    }

    String getString() {
        return this.s;
    }
}
