package org.mpierce.cs136.example.exceptions;

import java.io.IOException;

public class ExceptionsMain {

    // IOException is checked (parent class is Exception), so it must be declared in the method signature
    static String loadDataFromDisk() throws IOException {
        throw new IOException("Hardware failure");
    }

    static double squareRoot(int num) {
        if (num < 0) {
            throw new IllegalArgumentException("Input must be non negative");
        }

        return Math.sqrt(num);
    }

    // main can have org.mpierce.cs136.example.exceptions declared
    public static void main(String[] args) throws IOException {
        // This will throw and crash the main thread
        // The default behavior is to print a stack trace to stderr when a thread crashes
        System.out.println(loadDataFromDisk());
        System.out.println(squareRoot(3));
    }
}
