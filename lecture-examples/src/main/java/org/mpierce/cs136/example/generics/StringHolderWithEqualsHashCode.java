package org.mpierce.cs136.example.generics;

import java.util.Objects;

public class StringHolderWithEqualsHashCode {

    private final String s;

    public StringHolderWithEqualsHashCode(String s) {
        this.s = s;
    }

    String getString() {
        return this.s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        StringHolderWithEqualsHashCode that = (StringHolderWithEqualsHashCode) o;
        return Objects.equals(s, that.s);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s);
    }
}
