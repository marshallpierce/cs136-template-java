package org.mpierce.cs136.example.generics;

/**
 * Holds an object without generics to show casting and why it's unpleasant.
 */
class PlainBox {
    private final Object thing;

    PlainBox(Object thing) {
        this.thing = thing;
    }

    Object getThing() {
        return this.thing;
    }
}
